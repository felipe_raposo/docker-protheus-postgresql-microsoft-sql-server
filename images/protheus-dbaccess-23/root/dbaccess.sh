#!/bin/bash

# Inicia serviço do dbAccess.
export LC_ALL=C
export LD_LIBRARY_PATH=$LD_LIBRARY_PATH:.

sed 's/{{DB_Name}}/'"${DB_Name:-"protheus"}"'/g'         -i /dbaccess/multi/dbaccess.ini
sed 's/{{DB_User}}/'"${DB_User:-"protheus"}"'/g'         -i /dbaccess/multi/dbaccess.ini
sed 's/{{DB_Password}}/'"${DB_Password:-"protheus"}"'/g' -i /dbaccess/multi/dbaccess.ini

cd /dbaccess/multi/
./dbaccess64
