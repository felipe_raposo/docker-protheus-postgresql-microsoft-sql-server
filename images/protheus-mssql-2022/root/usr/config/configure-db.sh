#!/bin/bash

DBSTATUS=1
ERRCODE=1
i=0

# Create a symlink to maintain compatibility.
if [[ ! -d  /opt/mssql-tools/ && -d /opt/mssql-tools18/ ]]; then
	ln -s /opt/mssql-tools18/ /opt/mssql-tools
fi

# Wait 60 seconds for SQL Server to start up by ensuring that
# calling SQLCMD does not return an error code, which will ensure that sqlcmd is accessible
# and that system and user databases return "0" which means all databases are in an "online" state
# https://docs.microsoft.com/en-us/sql/relational-databases/system-catalog-views/sys-databases-transact-sql?view=sql-server-2017
while [[ $ERRCODE -ne 0 || $DBSTATUS -ne 0 ]] && [[ $i -lt 60 ]]; do
	i=$i+1
	DBSTATUS=$(/opt/mssql-tools/bin/sqlcmd -C -h -1 -t 1 -U sa -P $MSSQL_SA_PASSWORD -Q "SET NOCOUNT ON; select SUM(state) from sys.databases")
	ERRCODE=$?
	sleep 1
done

if [[ $DBSTATUS -ne 0 || $ERRCODE -ne 0 ]]; then
	if [[ $i -ge 60 ]]; then
		echo "SQL Server took more than 60 seconds to start up or one or more databases are not in an ONLINE state"
	fi
	exit 1
fi

# Run the setup script to create the DB and the schema in the DB
DATABASE_NAME=${DB_Name:-"protheus"}
SQL_ID=$(/opt/mssql-tools/bin/sqlcmd -C -h -1 -U sa -P $MSSQL_SA_PASSWORD -d master -Q "SET NOCOUNT ON; select isnull(DB_ID('$DATABASE_NAME'), 0)")
if [[ SQL_ID -eq 0 ]]; then
	echo "Creating $DATABASE_NAME database"
	DATABASE_PATH=$(realpath ${MSSQL_DATA_DIR:-"/var/opt/mssql/data"})
	sed 's@{{MSSQL_DATA_DIR}}@'"${DATABASE_PATH}"'@g'        -i /usr/config/setup.sql
	sed 's@{{DB_Name}}@'"${DATABASE_NAME}"'@g'               -i /usr/config/setup.sql
	sed 's@{{DB_User}}@'"${DB_User:-"protheus"}"'@g'         -i /usr/config/setup.sql
	sed 's@{{DB_Password}}@'"${DB_Password:-"protheus"}"'@g' -i /usr/config/setup.sql
	/opt/mssql-tools/bin/sqlcmd -C -U sa -P $MSSQL_SA_PASSWORD -d master -i /usr/config/setup.sql
else
	echo "Database $DATABASE_NAME already exists"
fi
