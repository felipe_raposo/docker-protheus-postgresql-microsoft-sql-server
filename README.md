# Ambiente Protheus 12 com PostgreSQL ou Microsoft SQL Server

Esse projeto visa a montagem rápida de um ambiente Protheus 12 com PostgreSQL 15 ou Microsoft SQL Server 2022 (últimas versões homologada de banco de dados).

As imagens aqui usadas são mantidas por mim, Felipe Raposo (feliperaposo@gmail.com), mas contribuições serão aceitas.

O projeto são basicamente cinco imagens:  
- PostgreSQL 15  
- Microsof SQL Server 2022  
- License Server (versão 3.6.0)  
- DbAccess (versão 23.1.1.5)
- Protheus Server (12.1.2310 - AppServer 20.3.2.12 - WebApp 9.1.6 - WebAgent 1.0.9 - Lib 01072024)

## Montar o ambiente

Basta baixar os arquivos [docker-compose.yaml e .env](https://bitbucket.org/felipe_raposo/docker-protheus-postgresql-microsoft-sql-server/src/master/compose/), e executar o comando:

```bash
docker-compose up  
```
Assim irá exibir os logs dos quatro containeres no console.

ou  

```bash
docker-compose up -d
```
Já com o parâmetro -d (detached) o console é liberado.

Será criado o diretório ./volume que conterá todos os dados do banco de dados e uma pasta temporária que será compartilhada com o AppServer, dentro de protheus_data/system_temp. Caso esteja sendo usado o SELinux, crie o diretório previamente e configure a label svirt_sandbox_file_t nesse diretório, com os comandos abaixo:

```bash
mkdir ./volume && chcon -t svirt_sandbox_file_t ./volume
```

## Portas disponíveis

Após a subida do ambiente, estarão disponíveis os serviços/portas:

- Porta 5432 (PostgreSQL) ou 1433 (Microsoft SQL Server)  
	Database: protheus / Usuário: protheus / Senha: protheus  
- Porta 7890 - dbAccess (sem senha)  
- Porta 1234 - Protheus (base vazia - usuário administrador sem senha)  
- Porta 8080 - WebApp do Protheus
- Porta 8081 - WebApp do LicenseServer
